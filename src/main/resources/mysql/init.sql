START TRANSACTION;
CREATE DATABASE IF NOT EXISTS jdbcEjemplo;

USE jdbcEjemplo;

CREATE TABLE IF NOT EXISTS Client(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    phone VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS Product(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    description VARCHAR(255),
    cost DECIMAL(19,2)
);

CREATE TABLE IF NOT EXISTS Sale(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    idClient INT UNSIGNED,
    total DECIMAL(19,2) DEFAULT 0,
    FOREIGN KEY(idClient) REFERENCES Client(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS SaleDetail(
    id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    idSale INT UNSIGNED,
    idProduct INT UNSIGNED,
    quantity INT,
    subtotal DECIMAL(19,2),
    FOREIGN KEY(idSale) REFERENCES Sale(id) ON DELETE RESTRICT ON UPDATE CASCADE,
    FOREIGN KEY(idProduct) REFERENCES Product(id) ON DELETE RESTRICT ON UPDATE CASCADE
);

DROP PROCEDURE IF EXISTS SP_CREATE_CLIENT;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE SP_CREATE_CLIENT (
    IN cname VARCHAR(255),
    IN cphone VARCHAR(255),
    OUT out_id INT,
    OUT out_name VARCHAR(255),
    OUT out_phone VARCHAR(255)
)
BEGIN
    DECLARE clientExists INT;

    SELECT COUNT(*)
    INTO clientExists
    FROM Client c
    WHERE c.phone = cphone ;

    IF clientExists > 0 THEN
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'The client is already in use';
    END IF;

    INSERT INTO Client (name,phone)
    VALUES(cname,cphone);

    SET out_id = LAST_INSERT_ID();
    SET out_name = cname;
    SET out_phone = cphone;

    COMMIT;
END$$

DROP PROCEDURE IF EXISTS SP_CREATE_PRODUCT;
CREATE DEFINER=`root`@`localhost` PROCEDURE SP_CREATE_PRODUCT (
    IN pdescription VARCHAR(255),
    IN pcost DOUBLE(19,2),
    OUT out_id INT,
    OUT out_description VARCHAR(255),
    OUT out_cost DOUBLE(19,2)
)
BEGIN
    INSERT INTO Product (description,cost)
    VALUES(pdescription,pcost);

    SET out_id = LAST_INSERT_ID();
    SET out_description = pdescription;
    SET out_cost = pcost;

    COMMIT;
END$$

DROP PROCEDURE IF EXISTS SP_CREATE_SALE;
CREATE DEFINER=`root`@`localhost` PROCEDURE SP_CREATE_SALE(
    IN input_data JSON,
    OUT result VARCHAR(255)
)
BEGIN
    DECLARE i INT DEFAULT 0;
    DECLARE num_items INT;
    DECLARE productExists INT;
    DECLARE currentId INT;
    DECLARE clientId INT;
    DECLARE last_sale_id INT;
    DECLARE totalSale INT DEFAULT 0;
    DECLARE clientExists INT DEFAULT 0;

    SET num_items = JSON_LENGTH(input_data, '$.saleDetailsDTO');
    SET clientId = JSON_EXTRACT(input_data,'$.idClient');

    SELECT COUNT(*)
    INTO clientExists FROM Client WHERE id = clientId;

    IF clientExists = 0 THEN
        SET @message_text = '';
        SET @message_text = CONCAT('The client with id ',clientId,' does not exist');
        SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = @message_text;
    END IF;

    WHILE i < num_items DO

        SET @productId = CONCAT('$.saleDetailsDTO[', i, '].idProduct');
        SET @currentId = JSON_EXTRACT(input_data,@productId);

        SELECT COUNT(*)
        INTO productExists
        FROM Product
        WHERE id = @currentId;

        IF productExists = 0 THEN
            SET @message_text = '';
            SET @message_text = CONCAT('Product with id ', @currentId, ' does not exist');
            SIGNAL SQLSTATE '45000'
            SET MESSAGE_TEXT = @message_text;
        END IF;
        SET i = i + 1;
    END WHILE;

    INSERT INTO Sale(idClient)
    VALUES (clientId);

    SET last_sale_id = LAST_INSERT_ID();
    SET i = 0;

    WHILE i < num_items DO
        SET @productId = CONCAT('$.saleDetailsDTO[', i, '].idProduct');
        SET @currentId = JSON_EXTRACT(input_data,@productId);

        SET @quantity = CONCAT('$.saleDetailsDTO[', i, '].quantity');
        SET @quantityPath = JSON_EXTRACT(input_data,@quantity);

        SET @productCost = 0;
        SELECT
        cost INTO @productCost
        FROM Product
        WHERE id = @currentId;

        SET @subtotal = ROUND(@productCost * @quantityPath, 2);
        SET totalSale = totalSale + @subtotal;

        INSERT INTO SaleDetail(idSale,idProduct,quantity,subtotal) VALUES (last_sale_id,@currentId,@quantityPath,@subtotal);
        SET i = i + 1;
    END WHILE;

    UPDATE Sale SET total = ROUND(totalSale, 2) WHERE id = last_sale_id;

    SET result = 'has been processed';
    COMMIT;
END$$

DROP PROCEDURE IF EXISTS SP_GET_SALES;
CREATE DEFINER=`root`@`localhost` PROCEDURE SP_GET_SALES( OUT json_result TEXT )
BEGIN
    DECLARE nSales INT DEFAULT 0;


    SELECT COUNT(*)
    INTO nSales
    FROM Sale;


END$$
DELIMITER ;
COMMIT;