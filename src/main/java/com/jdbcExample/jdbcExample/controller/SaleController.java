package com.jdbcExample.jdbcExample.controller;

import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.domain.model.Sale;
import com.jdbcExample.jdbcExample.dto.ProductDTO;
import com.jdbcExample.jdbcExample.dto.SaleDTO;
import com.jdbcExample.jdbcExample.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/sale")
public class SaleController {
	@Autowired
	private SaleService saleService;

	@GetMapping
	public ResponseEntity<List<Sale>> getSales() {
		return ResponseEntity.status(HttpStatus.OK).body(this.saleService.getSales());
	}

	@PostMapping
	public ResponseEntity<?> createSale(@RequestBody SaleDTO saleDTO){
		Map<String,Object> body = new HashMap<>();
		body.put("msg",this.saleService.createSale(saleDTO));
		body.put("ok",true);
		return ResponseEntity.status(HttpStatus.OK).body(body);
	}
}
