package com.jdbcExample.jdbcExample.controller;

import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.domain.responses.GetClientsResponse;
import com.jdbcExample.jdbcExample.dto.ClientDTO;
import com.jdbcExample.jdbcExample.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/client")
public class ClientController {

	@Autowired
	private ClientService clientService;
	@GetMapping
	public ResponseEntity<GetClientsResponse> getAll(){
		GetClientsResponse body = GetClientsResponse.builder().clients(this.clientService.getAll()).build();
		return ResponseEntity.status(HttpStatus.OK).body(body);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Client> getById(@PathVariable(name = "id") Long id){
		return ResponseEntity.status(HttpStatus.OK).body(this.clientService.getById(id));
	}

	@PostMapping
	public ResponseEntity<Client> save(@RequestBody ClientDTO clientDTO){
		return ResponseEntity.status(HttpStatus.OK).body(this.clientService.save(clientDTO));
	}
}
