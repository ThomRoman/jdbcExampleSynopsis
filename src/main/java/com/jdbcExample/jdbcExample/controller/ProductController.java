package com.jdbcExample.jdbcExample.controller;

import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.domain.responses.GetProductsResponse;
import com.jdbcExample.jdbcExample.dto.ProductDTO;
import com.jdbcExample.jdbcExample.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
	@Autowired
	private ProductService productService;

	@GetMapping
	public ResponseEntity<GetProductsResponse> getAll(){
		GetProductsResponse body = GetProductsResponse.builder().products(this.productService.getAll()).build();
		return ResponseEntity.status(HttpStatus.OK).body(body);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Product> getById(@PathVariable(name = "id") Long id){
		return ResponseEntity.status(HttpStatus.OK).body(this.productService.getById(id));
	}

	@PostMapping
	public ResponseEntity<Product> save(@RequestBody ProductDTO productDTO){
		return ResponseEntity.status(HttpStatus.OK).body(this.productService.save(productDTO));
	}
}
