package com.jdbcExample.jdbcExample.controller;

import com.jdbcExample.jdbcExample.errors.ClientBadRequestErrorException;
import com.jdbcExample.jdbcExample.errors.ClientFoundErrorException;
import com.jdbcExample.jdbcExample.errors.ProductErrorException;
import com.jdbcExample.jdbcExample.errors.SaleErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class HandlerErrorController {
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	@ExceptionHandler(ClientFoundErrorException.class)
	public Map<String,Object> handleClientFoundExceptions(ClientFoundErrorException ex){
		Map<String,Object> errors = new HashMap<>();
		errors.put("ok",false);
		errors.put("msg",ex.getMessage());
		return errors;
	}

	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ClientBadRequestErrorException.class)
	public Map<String,Object> handleClientBadRequestExceptions(ClientBadRequestErrorException ex){
		Map<String,Object> errors = new HashMap<>();
		errors.put("ok",false);
		errors.put("msg",ex.getMessage());
		return errors;
	}

	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(ProductErrorException.class)
	public Map<String,Object> handleProductErrorExceptions(ProductErrorException ex){
		Map<String,Object> errors = new HashMap<>();
		errors.put("ok",false);
		errors.put("msg",ex.getMessage());
		return errors;
	}


	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(SaleErrorException.class)
	public Map<String,Object> handleSaleErrorExceptions(SaleErrorException ex){
		Map<String,Object> errors = new HashMap<>();
		errors.put("ok",false);
		errors.put("msg",ex.getMessage());
		return errors;
	}
}
