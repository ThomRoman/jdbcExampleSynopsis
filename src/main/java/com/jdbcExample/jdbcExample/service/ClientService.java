package com.jdbcExample.jdbcExample.service;

import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.dto.ClientDTO;

import java.util.List;

public interface ClientService {
	List<Client> getAll();

	Client save(ClientDTO clientDTO);

	Client getById(Long id);

	int operation(int a,int b,String operacion);
}
