package com.jdbcExample.jdbcExample.service;

import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.dto.ProductDTO;

import java.util.List;

public interface ProductService {
	List<Product> getAll();

	Product save(ProductDTO productDTO);

	Product getById(Long id);
}
