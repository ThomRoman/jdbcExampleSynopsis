package com.jdbcExample.jdbcExample.service.impl;

import com.jdbcExample.jdbcExample.repository.ProductRepository;
import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.dto.ProductDTO;
import com.jdbcExample.jdbcExample.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl extends ProductDTO implements ProductService {

	@Autowired
	private ProductRepository productRepository;

	@Override
	public List<Product> getAll() {
		return this.productRepository.getAll();
	}

	@Override
	public Product save(ProductDTO productDTO) {
		return this.productRepository.save(productDTO);
	}

	@Override
	public Product getById(Long id) {
		return this.productRepository.getById(id);
	}
}
