package com.jdbcExample.jdbcExample.service.impl;

import com.jdbcExample.jdbcExample.repository.ClientRepository;
import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.dto.ClientDTO;
import com.jdbcExample.jdbcExample.service.ClientService;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository clientRepository;

	@Override
	public List<Client> getAll() {
		return this.clientRepository.getAll();
	}

	@Override
	public Client save(ClientDTO clientDTO) {
		return this.clientRepository.save(clientDTO);
	}

	@Override
	public Client getById(Long id) {
		return this.clientRepository.getById(id);
	}

	@Override
	public int operation(int a, int b,String operacion) {
		if(operacion.equals("suma")) return a + b;
		else if(operacion.equals("multi")) return this.clientRepository.multiplicar(a,b);
		return -1000;
	}
}
