package com.jdbcExample.jdbcExample.service.impl;

import com.jdbcExample.jdbcExample.repository.ClientRepository;
import com.jdbcExample.jdbcExample.repository.ProductRepository;
import com.jdbcExample.jdbcExample.repository.SaleDetailRepository;
import com.jdbcExample.jdbcExample.repository.SaleRepository;
import com.jdbcExample.jdbcExample.domain.model.Sale;
import com.jdbcExample.jdbcExample.domain.model.SaleDetail;
import com.jdbcExample.jdbcExample.dto.SaleDTO;
import com.jdbcExample.jdbcExample.service.SaleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleServiceImpl implements SaleService {

	private final Logger log = LoggerFactory.getLogger(SaleServiceImpl.class);
	@Autowired
	private SaleRepository saleRepository;

	@Autowired
	private SaleDetailRepository saleDetailRepository;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private ProductRepository productRepository;


	@Override
	public List<Sale> getSales() {
		List<Sale> sales = saleRepository.getSales();
		for(Sale sale : sales) {
			List<SaleDetail> saleDetails = saleDetailRepository.getSaleDetailsBySaleId(String.valueOf(sale.getId()));
			log.info(saleDetails.toString());
			for(SaleDetail saleDetail : saleDetails) {
				saleDetail.setProduct( productRepository.getById( saleDetail.getProduct().getId()) );
			}
			sale.setSaleDetails(saleDetails);
			sale.setClient(clientRepository.getById(sale.getClient().getId()));
		}
		return sales;

	}

	@Override
	public String createSale(SaleDTO saleDTO) {
		return this.saleRepository.createSale(saleDTO);
	}
}
