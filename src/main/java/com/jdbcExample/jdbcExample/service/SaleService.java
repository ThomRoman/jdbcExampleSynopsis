package com.jdbcExample.jdbcExample.service;

import com.jdbcExample.jdbcExample.domain.model.Sale;
import com.jdbcExample.jdbcExample.dto.SaleDTO;

import java.util.List;

public interface SaleService {
	List<Sale> getSales();

	String createSale(SaleDTO saleDTO);
}
