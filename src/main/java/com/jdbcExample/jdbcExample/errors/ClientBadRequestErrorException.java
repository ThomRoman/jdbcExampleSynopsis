package com.jdbcExample.jdbcExample.errors;

public class ClientBadRequestErrorException extends RuntimeException{
	public ClientBadRequestErrorException(String msg){
		super(msg);
	}
}
