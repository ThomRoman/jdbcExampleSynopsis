package com.jdbcExample.jdbcExample.errors;

public class ClientFoundErrorException extends RuntimeException{
	public ClientFoundErrorException(String msg){
		super(msg);
	}
}
