package com.jdbcExample.jdbcExample.errors;

public class ProductErrorException extends RuntimeException{
	public ProductErrorException(String msg){
		super(msg);
	}
}
