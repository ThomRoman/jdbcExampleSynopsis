package com.jdbcExample.jdbcExample.errors;

public class SaleErrorException extends RuntimeException{
	public SaleErrorException(String msg){
		super(msg);
	}
}