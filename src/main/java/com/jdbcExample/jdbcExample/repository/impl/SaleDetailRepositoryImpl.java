package com.jdbcExample.jdbcExample.repository.impl;

import com.jdbcExample.jdbcExample.repository.SaleDetailRepository;
import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.domain.model.SaleDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class SaleDetailRepositoryImpl implements SaleDetailRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<SaleDetail> getSaleDetailsBySaleId(String saleId) {
		String sql = "SELECT * FROM SaleDetail WHERE idSale = ?";
		List<SaleDetail> listSaleDetails = this.jdbcTemplate.query(sql,
				(rs, rowNum) -> mapToSaleDetail(rs), saleId);
		return listSaleDetails;
	}
	private SaleDetail mapToSaleDetail(ResultSet rs) throws SQLException {
		SaleDetail saleDetail = SaleDetail.builder()
				.id(rs.getLong("id"))
				.build();
		Product product = Product.builder()
				.id(rs.getLong("idProduct"))
				.build();
		saleDetail.setQuantity(rs.getInt("quantity"));
		saleDetail.setSubtotal(rs.getDouble("subtotal"));
		saleDetail.setProduct(product);
		return saleDetail;
	}
}
