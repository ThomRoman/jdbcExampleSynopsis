package com.jdbcExample.jdbcExample.repository.impl;

import com.jdbcExample.jdbcExample.repository.ClientRepository;
import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.dto.ClientDTO;
import com.jdbcExample.jdbcExample.errors.ClientBadRequestErrorException;
import com.jdbcExample.jdbcExample.errors.ClientFoundErrorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

@Repository
public class ClientRepositoryImpl implements ClientRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private final Logger log = LoggerFactory.getLogger(SaleRepositoryImpl.class);
	@Override
	public List<Client> getAll() {
		String sql = "SELECT * FROM Client";
		List<Client> clients = this.jdbcTemplate.query(sql, (resultSet, rowNum) -> {
			return Client.builder()
					.id(resultSet.getLong("id"))
					.name(resultSet.getString("name"))
					.phone(resultSet.getString("phone"))
					.build();
		});
		return clients;
	}

	@Override
	public Client save(ClientDTO clientDTO) {
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName("SP_CREATE_CLIENT")
					.declareParameters(
							new SqlParameter("cname", Types.VARCHAR),
							new SqlParameter("cphone", Types.VARCHAR),
							new SqlOutParameter("out_id", Types.INTEGER),
							new SqlOutParameter("out_name", Types.VARCHAR),
							new SqlOutParameter("out_phone", Types.VARCHAR)
					);

			MapSqlParameterSource inParams = new MapSqlParameterSource();
			inParams.addValue("cname", clientDTO.getName());
			inParams.addValue("cphone", clientDTO.getPhone());

			Map<String, Object> outParams = jdbcCall.execute(inParams);

			Client newClient = Client.builder()
					.id(((Number) outParams.get("out_id")).longValue())
					.name((String) outParams.get("out_name"))
					.phone((String) outParams.get("out_phone"))
					.build();

			return newClient;
		}catch (DataAccessException e){
			log.info(e.getMessage());
			Throwable cause = e.getCause();
			if (cause instanceof SQLException) {
				SQLException sqlException = (SQLException) cause;
				if ("45000".equals(sqlException.getSQLState())) {
					throw new ClientBadRequestErrorException("Error al crear una cliente => "+sqlException.getMessage());
				}
			}
			throw new ClientBadRequestErrorException("An error occurred while creating the user");
		}
	}

	@Override
	public Client getById(Long id) {
		String sql = "SELECT * FROM Client WHERE id=?";
		try {
			return jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, rowNum) -> {
				Client client = Client.builder()
						.id(resultSet.getLong("id"))
						.name(resultSet.getString("name"))
						.phone(resultSet.getString("phone"))
						.build();
				return client;
			});
		} catch (EmptyResultDataAccessException e) {
			throw new ClientFoundErrorException("The client with id " + id+" does not exit");
		}
	}

	@Override
	public int multiplicar(int a, int b) {
		return a * b;
	}
}
