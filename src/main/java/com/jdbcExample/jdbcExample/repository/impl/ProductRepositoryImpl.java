package com.jdbcExample.jdbcExample.repository.impl;

import com.jdbcExample.jdbcExample.repository.ProductRepository;
import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.dto.ProductDTO;
import com.jdbcExample.jdbcExample.errors.ProductErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;

@Repository
public class ProductRepositoryImpl implements ProductRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Override
	public List<Product> getAll() {
		String sql = "SELECT * FROM Product";
		List<Product> products = this.jdbcTemplate.query(sql, (resultSet, rowNum) -> {
			return Product.builder()
					.id(resultSet.getLong("id"))
					.description(resultSet.getString("description"))
					.cost(resultSet.getDouble("cost"))
					.build();
		});
		return products;
	}
//
	@Override
	public Product save(ProductDTO productDTO) {
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName("SP_CREATE_PRODUCT")
					.declareParameters(
							new SqlParameter("pdescription", Types.VARCHAR),
							new SqlParameter("pcost", Types.VARCHAR),
							new SqlOutParameter("out_id", Types.INTEGER),
							new SqlOutParameter("out_description", Types.VARCHAR),
							new SqlOutParameter("out_cost", Types.VARCHAR)
					);

			MapSqlParameterSource inParams = new MapSqlParameterSource();
			inParams.addValue("pdescription", productDTO.getDescription());
			inParams.addValue("pcost", productDTO.getCost());

			Map<String, Object> outParams = jdbcCall.execute(inParams);

			Product newProduct = Product.builder()
					.id(((Number) outParams.get("out_id")).longValue())
					.description((String) outParams.get("out_description"))
					.cost(Double.parseDouble((String) outParams.get("out_cost")))
					.build();

			return newProduct;
		}catch (DataAccessException e){
			Throwable cause = e.getCause();
			if (cause instanceof SQLException) {
				SQLException sqlException = (SQLException) cause;
				if ("45000".equals(sqlException.getSQLState())) {
					throw new ProductErrorException("Error al crear un product => "+sqlException.getMessage());
				}
			}
			throw new ProductErrorException("An error occurred while creating the product");
		}
	}

	@Override
	public Product getById(Long id) {
		String sql = "SELECT * FROM Product WHERE id=?";
		try {
			return jdbcTemplate.queryForObject(sql, new Object[]{id}, (resultSet, rowNum) -> {
				Product product = Product.builder()
						.id(resultSet.getLong("id"))
						.description(resultSet.getString("description"))
						.cost(resultSet.getDouble("cost"))
						.build();
				return product;
			});
		} catch (EmptyResultDataAccessException e) {
			throw new ProductErrorException("The Product with id " + id+" does not exit");
		}
	}
}
