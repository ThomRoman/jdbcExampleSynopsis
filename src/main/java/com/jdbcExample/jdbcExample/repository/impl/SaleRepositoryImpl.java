package com.jdbcExample.jdbcExample.repository.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jdbcExample.jdbcExample.repository.SaleRepository;
import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.domain.model.Sale;
import com.jdbcExample.jdbcExample.dto.SaleDTO;
import com.jdbcExample.jdbcExample.errors.SaleErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class SaleRepositoryImpl implements SaleRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private final Logger log = LoggerFactory.getLogger(SaleRepositoryImpl.class);

	@Override
	public List<Sale> getSales() {
		String sql = "SELECT * FROM Sale";
		List<Sale> sales = jdbcTemplate.query(sql,
				(rs, rowNum) -> mapToSale(rs));
		return sales;

	}

	private Sale mapToSale(ResultSet rs) throws SQLException {
		Sale sale = Sale.builder().id(rs.getLong("id")).total(rs.getDouble("total")).build();
		Client client = Client.builder().id(rs.getLong("idClient")).build();
		sale.setClient(client);
		return sale;
	}

	@Override
	public String createSale(SaleDTO saleDTO) {
		ObjectMapper objectMapper = new ObjectMapper();
		String inputJson = null;
		try {
			inputJson = objectMapper.writeValueAsString(saleDTO);
		} catch (JsonProcessingException e) {
			throw new SaleErrorException("Incorrect JSON");
		}
		try{
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate)
					.withProcedureName("SP_CREATE_SALE")
					.declareParameters(
							new SqlParameter("input_data", Types.VARCHAR),
							new SqlOutParameter("result", Types.VARCHAR)
					);
			Map<String, Object> inParams = Map.of("input_data", inputJson);
			Map<String, Object> outParams = jdbcCall.execute(inParams);
			var result = (String) outParams.get("result");
			log.info(result);
			return result;
		}catch (DataAccessException e){
			Throwable cause = e.getCause();
			if (cause instanceof SQLException) {
				SQLException sqlException = (SQLException) cause;
				if ("45000".equals(sqlException.getSQLState())) {
					throw new SaleErrorException("Error al crear un Sale => "+sqlException.getMessage());
				}
			}
			throw new SaleErrorException("An error occurred while creating the sale");
		}
	}
}
