package com.jdbcExample.jdbcExample.repository;

import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.dto.ClientDTO;

import java.util.List;

public interface ClientRepository {
	List<Client> getAll();

	Client save(ClientDTO clientDTO);

	Client getById(Long id);

	int multiplicar(int a,int b);
}
