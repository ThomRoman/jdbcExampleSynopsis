package com.jdbcExample.jdbcExample.repository;

import com.jdbcExample.jdbcExample.domain.model.SaleDetail;

import java.util.List;

public interface SaleDetailRepository {
	List<SaleDetail> getSaleDetailsBySaleId(String saleId);
}
