package com.jdbcExample.jdbcExample.repository;

import com.jdbcExample.jdbcExample.domain.model.Sale;
import com.jdbcExample.jdbcExample.dto.SaleDTO;

import java.util.List;

public interface SaleRepository {
	List<Sale> getSales();

	String createSale(SaleDTO saleDTO);
}
