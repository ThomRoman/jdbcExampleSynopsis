package com.jdbcExample.jdbcExample.repository;

import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.dto.ProductDTO;

import java.util.List;

public interface ProductRepository {
	List<Product> getAll();

	Product save(ProductDTO productDTO);

	Product getById(Long id);
}
