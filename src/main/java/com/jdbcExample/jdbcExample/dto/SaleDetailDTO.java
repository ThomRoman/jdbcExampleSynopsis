package com.jdbcExample.jdbcExample.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleDetailDTO {
	private Long idProduct;
	private int quantity;
}
