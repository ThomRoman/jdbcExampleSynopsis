package com.jdbcExample.jdbcExample.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleDTO {
	private Long idClient;
	private List<SaleDetailDTO> saleDetailsDTO;
}
