package com.jdbcExample.jdbcExample.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaleDetail {
	private Long id;
	private Product product;
	private int quantity;
	private Double subtotal;
}
