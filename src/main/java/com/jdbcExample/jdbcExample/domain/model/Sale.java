package com.jdbcExample.jdbcExample.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sale {
	private Long id;
	private Client client;

	private double total;

	private List<SaleDetail> saleDetails;
}
