package com.jdbcExample.jdbcExample.domain.responses;

import com.jdbcExample.jdbcExample.domain.model.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetProductsResponse {
	List<Product> products;
}
