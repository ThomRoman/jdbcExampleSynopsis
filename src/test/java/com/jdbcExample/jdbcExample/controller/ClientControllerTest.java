package com.jdbcExample.jdbcExample.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.dto.ClientDTO;
import com.jdbcExample.jdbcExample.errors.ClientFoundErrorException;
import com.jdbcExample.jdbcExample.service.ClientService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

// WebMvcTest : nos servira para testear los controladores, cargara el controlador especifico
// tambien autoconfigurara mock mvc, para realizar reticiones http a nuestro controlador

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.hamcrest.CoreMatchers.is;

// son como postman prueba de integracion donde probara toda la app
// pero la app tiene que estara levantada, en un puerto la app y otra el test
//WebTestClient: (asincronas) con webflux, no bloqueantes, realizar varias peticiones e ir procesando las respuesta y devolver (MongoDB)
//TestRestTemplate:  peticiones que son bloqueantes (cliente bloqueante) hace una peticion y espera una respuesta, este hace una peticion y bloquea
// se tiene que esperar a que termine de procesar para hacer otra request (Mysql)

@WebMvcTest(ClientController.class)
public class ClientControllerTest {

	// petticiones http
	@Autowired
	private MockMvc mockMvc;

	// mockbean, agregar objetos simulados al contexto de la app
	@MockBean
	private ClientService clientService;

	// parsear objetios json
	@Autowired
	private ObjectMapper objectMapper;

	// el throw es poe el objectMapper
	@Test
	public void saveTest() throws Exception{
		// given
		ClientDTO clientDTO = ClientDTO.builder().name("Thom").phone("123456789").build();
		Client client = Client.builder().id(1L).name("Thom").phone("123456789").build();

		// configuracion del comportamiento simulado
		// esto significa que cuando se llama al metodo save de clientService con cualquier parametro del tipo clientDto
		// respondera con el mismo objeto que se le envie (invocation)->invocation.getArgument(0)
		//given(clientService.save(any(ClientDTO.class))).willAnswer((invocation)->invocation.getArgument(0));

		given(clientService.save(any(ClientDTO.class))).willReturn(client);

		// when
		// acciones
		// haciendo el requets
		// se envia el clientDto al controlador este usa el servicio y ocurre lo comentado antes
		ResultActions response = mockMvc.perform(
				post("/api/v1/client")
						.contentType(MediaType.APPLICATION_JSON)
						.content(objectMapper.writeValueAsString(clientDTO))
		);


		// then
		// imprime en consola, la respuesta
		response.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.name",is(client.getName())))
				.andExpect(jsonPath("$.phone",is(client.getPhone())));
	}

	@Test
	public void getByIdNotFound() throws  Exception{
		Long id = 156L;
		given(clientService.getById(id)).willThrow(ClientFoundErrorException.class);

		ResultActions response = mockMvc.perform(get("/api/v1/client/{id}",id));
		response.andExpect(status().isNotFound())
				.andDo(print());
	}


	@Test
	public void getAll() throws Exception{
		List<Client> clients = List.of(
				Client.builder().name("Thom").phone("123456789").id(1L).build(),
				Client.builder().name("Carlos").phone("987654321").id(2L).build()
		);
		given(clientService.getAll()).willReturn(clients);

		ResultActions response = mockMvc.perform(get("/api/v1/client"));
		response.andExpect(status().isOk())
				.andDo(print())
				.andExpect(jsonPath("$.clients.size()",is(clients.size())));
	}
}
