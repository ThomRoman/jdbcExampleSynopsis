package com.jdbcExample.jdbcExample.controller;

import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.dto.ProductDTO;
import com.jdbcExample.jdbcExample.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.when;
import static org.mockito.BDDMockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
@ExtendWith(MockitoExtension.class)
@DisplayName("Rest client controller")
public class ProductControllerTestv2 {

	@Mock
	private ProductServiceImpl productService;

	@InjectMocks
	private ProductController productController;


	@Test
	public void getById(){
		Product product = Product.builder().id(1L).cost(10).description("Tomate").build();
		given(productService.getById(1L)).willReturn(product);

		ResponseEntity<Product> responseEntity = productController.getById(1L);

		var response = responseEntity.getBody();
		assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
		assertNotNull(response);
		assertEquals(product,response);
	}
}
