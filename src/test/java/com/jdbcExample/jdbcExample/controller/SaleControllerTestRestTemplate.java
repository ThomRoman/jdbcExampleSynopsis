package com.jdbcExample.jdbcExample.controller;

import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.domain.model.Sale;
import com.jdbcExample.jdbcExample.dto.ClientDTO;
import com.jdbcExample.jdbcExample.dto.ProductDTO;
import com.jdbcExample.jdbcExample.dto.SaleDTO;
import com.jdbcExample.jdbcExample.dto.SaleDetailDTO;
import com.jdbcExample.jdbcExample.repository.ClientRepository;
import com.jdbcExample.jdbcExample.repository.ProductRepository;
import com.jdbcExample.jdbcExample.repository.impl.ClientRepositoryImpl;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // oara el orden de los metodos
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SaleControllerTestRestTemplate {
	@Autowired
	private TestRestTemplate testRestTemplate;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private ProductRepository productRepository;

	private Client client;

	private Product product1,product2;

	@LocalServerPort
	private int port;

	@BeforeAll
	public void setup(){
		jdbcTemplate.execute("DELETE FROM SaleDetail");
		jdbcTemplate.execute("DELETE FROM Sale");
		jdbcTemplate.execute("DELETE FROM Product");
		jdbcTemplate.execute("DELETE FROM Client");

		client = clientRepository.save(ClientDTO.builder().name("Thom").phone("123456789").build());
		product1 = productRepository.save(ProductDTO.builder().cost(10).description("lechuga").build());
		product2 = productRepository.save(ProductDTO.builder().cost(5).description("tomate").build());
	}

	@Test
	public void createSale(){
		SaleDTO saleDTO = SaleDTO.builder().saleDetailsDTO(List.of(
				SaleDetailDTO.builder().idProduct(product1.getId()).quantity(1).build(),
				SaleDetailDTO.builder().idProduct(product2.getId()).quantity(2).build()
		)).idClient(client.getId()).build();
		String baseUrl = "http://localhost:" + port;
		ResponseEntity<Sale> respuesta = testRestTemplate.postForEntity(baseUrl + "/api/v1/sale", saleDTO, Sale.class);
		assertEquals(HttpStatus.OK,respuesta.getStatusCode());
		assertEquals(MediaType.APPLICATION_JSON,respuesta.getHeaders().getContentType());
	}

	@AfterAll
	public void cleanup() {
		jdbcTemplate.execute("DELETE FROM SaleDetail");
		jdbcTemplate.execute("DELETE FROM Sale");
		jdbcTemplate.execute("DELETE FROM Product");
		jdbcTemplate.execute("DELETE FROM Client");
	}
}
