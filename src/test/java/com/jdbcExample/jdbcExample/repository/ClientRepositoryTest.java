package com.jdbcExample.jdbcExample.repository;

import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.repository.impl.ClientRepositoryImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ClientRepositoryTest {
	@Mock
	private JdbcTemplate jdbcTemplate;

	@InjectMocks
	private ClientRepositoryImpl clientRepository;

	private Client client1;
	private Client client2;
	private List<Client> clients;

	@BeforeEach
	public void setUp() {
		client1 = Client.builder()
				.id(1L)
				.name("John Doe")
				.phone("1234567890")
				.build();

		client2 = Client.builder()
				.id(2L)
				.name("Jane Doe")
				.phone("0987654321")
				.build();

		clients = Arrays.asList(client1, client2);
	}

	@Test
	public void testGetAll() {
		when(jdbcTemplate.query(anyString(), any(RowMapper.class))).thenReturn(clients);

		List<Client> result = clientRepository.getAll();

		assertEquals(clients, result);
		verify(jdbcTemplate, times(1)).query(anyString(), any(RowMapper.class));
	}
}
