package com.jdbcExample.jdbcExample.repository;
import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.dto.ClientDTO;
import com.jdbcExample.jdbcExample.errors.ClientFoundErrorException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;


@SpringBootTest
@ActiveProfiles("test")
public class ClientRepositoryTestWithoutMock {


	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private ClientRepository clientRepository;


	@BeforeEach
	public void setup() {
		jdbcTemplate.execute("DELETE FROM SaleDetail");
		jdbcTemplate.execute("DELETE FROM Sale");
		jdbcTemplate.execute("DELETE FROM Product");
		jdbcTemplate.execute("DELETE FROM Client");
	}

	@DisplayName("Test for save a client")
	@Test
	public void saveClientTest(){
		ClientDTO _clientDTO = new ClientDTO();
		_clientDTO.setName("Test Name");
		_clientDTO.setPhone("1234567890");

		//when - acción o el comportamiento que vamos a probar
		Client clientSaved = clientRepository.save(_clientDTO);

		//then - verificar la salida
		assertThat(clientSaved).isNotNull();
		assertThat(clientSaved.getId()).isGreaterThan(0);
		assertEquals(_clientDTO.getName(),clientSaved.getName());
	}

	@DisplayName("Test for list all the clients")
	@Test
	public void getAllTest(){

		ClientDTO _clientDTO1 = ClientDTO.builder()
				.name("John Doe")
				.phone("1234567890")
				.build();
		ClientDTO _ClientDTO2 = ClientDTO.builder()
				.name("Jane Doe")
				.phone("0987654321")
				.build();

		Client clientSaved1 = clientRepository.save(_clientDTO1);
		Client clientSaved2 = clientRepository.save(_ClientDTO2);
		List<Client> clients = Arrays.asList(clientSaved1,clientSaved2);
		var response = clientRepository.getAll();
		assertEquals(clients, response);
	}

	@DisplayName("Test for find client by id")
	@Test
	public void findByIdTest(){
		ClientDTO _clientDTO1 = ClientDTO.builder()
				.name("John Doe")
				.phone("1234567890")
				.build();
		Client clientSaved1 = clientRepository.save(_clientDTO1);
		Client found = clientRepository.getById(clientSaved1.getId());
		assertEquals(clientSaved1,found);
	}

	@DisplayName("Test throw error when not found client by id")
	@Test
	public void notFoundByIdTest(){
		assertThrows(ClientFoundErrorException.class, () -> {
			clientRepository.getById(123L);
		});
	}

	@AfterEach
	public void cleanup() {
		jdbcTemplate.execute("DELETE FROM SaleDetail");
		jdbcTemplate.execute("DELETE FROM Sale");
		jdbcTemplate.execute("DELETE FROM Product");
		jdbcTemplate.execute("DELETE FROM Client");
	}


}
