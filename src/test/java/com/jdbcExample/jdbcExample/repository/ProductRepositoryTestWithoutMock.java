package com.jdbcExample.jdbcExample.repository;

import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.dto.ProductDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.List;

@SpringBootTest
@ActiveProfiles("test")
public class ProductRepositoryTestWithoutMock {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private ProductRepository productRepository;

	@BeforeEach
	public void setup() {
		jdbcTemplate.execute("DELETE FROM SaleDetail");
		jdbcTemplate.execute("DELETE FROM Sale");
		jdbcTemplate.execute("DELETE FROM Product");
		jdbcTemplate.execute("DELETE FROM Client");
	}

	@Test
	public void getAllTest(){

		var productDto1 = ProductDTO.builder().cost(10).description("Tomate").build();

		var productDto2 = ProductDTO.builder().cost(20).description("Lechuga").build();



		var productSaved1 = productRepository.save(productDto1);
		var productSaved2 = productRepository.save(productDto2);

		List<Product> productsSaved = List.of(productSaved1,productSaved2);

		var listResponse = productRepository.getAll();

		assertEquals(productsSaved,listResponse);
	}

	@AfterEach
	public void cleanup() {
		jdbcTemplate.execute("DELETE FROM SaleDetail");
		jdbcTemplate.execute("DELETE FROM Sale");
		jdbcTemplate.execute("DELETE FROM Product");
		jdbcTemplate.execute("DELETE FROM Client");
	}
}
