package com.jdbcExample.jdbcExample.repository;

import com.jdbcExample.jdbcExample.domain.model.Sale;
import com.jdbcExample.jdbcExample.dto.ClientDTO;
import com.jdbcExample.jdbcExample.dto.ProductDTO;
import com.jdbcExample.jdbcExample.dto.SaleDTO;
import com.jdbcExample.jdbcExample.dto.SaleDetailDTO;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

@SpringBootTest
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SaleRepositoryTestWithoutMock {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private SaleRepository saleRepository;
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private ProductRepository productRepository;

	SaleDTO saleDTO;


	@BeforeAll
	void setUp() {
		ClientDTO clientDTO = ClientDTO.builder().name("Thom").phone("123456789").build();

		var clientSaved = clientRepository.save(clientDTO);

		ProductDTO productDTO1 = ProductDTO.builder().cost(10).description("Tomate").build();
		ProductDTO productDTO2 = ProductDTO.builder().cost(20).description("Lechuga").build();

		var productSaved1 = productRepository.save(productDTO1);
		var productSaved2 = productRepository.save(productDTO2);

		var saleDetail1 = SaleDetailDTO.builder().idProduct(productSaved1.getId()).quantity(2).build();
		var saleDetail2 = SaleDetailDTO.builder().idProduct(productSaved2.getId()).quantity(1).build();

		saleDTO = SaleDTO.builder().saleDetailsDTO(List.of(saleDetail1,saleDetail2)).idClient(clientSaved.getId()).build();
	}


	@Test
	public void createSaleTest(){
		var saleCreated = saleRepository.createSale(saleDTO);
		assertEquals("has been processed",saleCreated);
	}

	@Test
	public void getSalesTest(){
		var sales = saleRepository.getSales();
		assertEquals(1,sales.size());
		assertEquals(40,sales.get(0).getTotal());
	}

}
