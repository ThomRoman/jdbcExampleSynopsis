package com.jdbcExample.jdbcExample.domain.model;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class ModelTest {

	private Product product;
	private Client client;
	private Sale sale;
	private SaleDetail saleDetail;

	@BeforeEach
	void setup(){
		client = Client.builder()
				.name("Thom")
				.phone("123456789")
				.id(1L).build();
		product = Product.builder().cost(10.5).description("Pataata").id(1L).build();
		saleDetail = SaleDetail.builder().product(product).quantity(1).subtotal(10.5).id(1L).build();
		sale = Sale.builder().saleDetails(List.of(saleDetail)).total(10.5).client(client).id(1L).build();
	}

	@Test
	void clientTest(){

		assertEquals(1L,client.getId());
		assertEquals("Thom",client.getName());
	}

	@Test
	void productTest(){
		assertEquals(1L,product.getId());
		assertEquals("Pataata",product.getDescription());
	}

	@Test
	void saleTest(){
		assertEquals(10.5,sale.getTotal());
		assertEquals("Pataata",sale.getSaleDetails().get(0).getProduct().getDescription());
	}

	@Test
	void saleDetailTest(){
		assertEquals(1L,saleDetail.getProduct().getId());
		assertEquals("Pataata",saleDetail.getProduct().getDescription());
		assertEquals(10.5,saleDetail.getSubtotal());
	}
}
