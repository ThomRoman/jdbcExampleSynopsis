package com.jdbcExample.jdbcExample.service;

import com.jdbcExample.jdbcExample.domain.model.Client;
import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.domain.model.Sale;
import com.jdbcExample.jdbcExample.domain.model.SaleDetail;
import com.jdbcExample.jdbcExample.repository.ClientRepository;
import com.jdbcExample.jdbcExample.repository.ProductRepository;
import com.jdbcExample.jdbcExample.repository.SaleDetailRepository;
import com.jdbcExample.jdbcExample.repository.SaleRepository;
import com.jdbcExample.jdbcExample.service.impl.SaleServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.*;

@ExtendWith(MockitoExtension.class)
public class SaleServiceTest {

	@Mock
	private SaleDetailRepository saleDetailRepository;

	@Mock
	private SaleRepository saleRepository;

	@Mock
	private ClientRepository clientRepository;

	@Mock
	private ProductRepository productRepository;

	@InjectMocks
	private SaleServiceImpl saleServiceImpl;

	@DisplayName("Test to get empty sales")
	@Test
	public void getEmptySalesTest(){
		given(saleRepository.getSales()).willReturn(List.of());

		var salesSaved = saleServiceImpl.getSales();

		assertEquals(0,salesSaved.size());
	}

	@DisplayName("Test to get empty details into one sale")
	@Test
	public void getSalesTest(){
		Client client = Client.builder().id(1L).phone("123456789").name("Thom").build();
		Sale sale1 = Sale.builder().id(1L).client(Client.builder().id(1L).build()).total(10).build();

		Product product1 = Product.builder().id(1L).cost(5).description("Tomate").build();
		Product product2 = Product.builder().id(2L).cost(10).description("Lechuga").build();

		SaleDetail saleDetail1 = SaleDetail.builder().id(1L).product(Product.builder().id(1L).build()).build();
		SaleDetail saleDetail2 = SaleDetail.builder().id(2L).product(Product.builder().id(2L).build()).build();

		given(saleRepository.getSales()).willReturn(List.of(sale1));

		given(saleDetailRepository.getSaleDetailsBySaleId(String.valueOf(sale1.getId()))).willReturn(List.of(saleDetail1,saleDetail2));

		given(productRepository.getById(product1.getId())).willReturn(product1);
		given(productRepository.getById(product2.getId())).willReturn(product2);
		given(clientRepository.getById(sale1.getClient().getId())).willReturn(client);

		var sales = saleServiceImpl.getSales();

		assertThat(sales).isNotEmpty();
	}
}
