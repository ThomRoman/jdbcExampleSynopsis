package com.jdbcExample.jdbcExample.service;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.jdbcExample.jdbcExample.repository.impl.ClientRepositoryImpl;
import com.jdbcExample.jdbcExample.service.impl.ClientServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)

public class ClientServiceTest {

	@Mock
	ClientRepositoryImpl clientRepositoryImpl;


	@Test
	void sumaTest(){
		ClientService clsrv = ClientServiceImpl.builder().build();
		int a = 5,b = 6;
		assertEquals(11,clsrv.operation(a,b,"suma"));
	}

	@Test
	void defaultTest(){
		ClientService clsrv = ClientServiceImpl.builder().build();
		int a = 5,b = 6;
		assertEquals(-1000,clsrv.operation(a,b,""));
	}

	@Test
	void multiTest(){
		int a = 5,b = 5;

		when(clientRepositoryImpl.multiplicar(a,b)).thenReturn(25);

		ClientService clsrv = ClientServiceImpl.builder()
				.clientRepository(clientRepositoryImpl)
				.build();
		assertEquals(25,clsrv.operation(a,b,"multi"));
	}

}
