package com.jdbcExample.jdbcExample.service;

import com.jdbcExample.jdbcExample.domain.model.Product;
import com.jdbcExample.jdbcExample.dto.ProductDTO;
import com.jdbcExample.jdbcExample.repository.ProductRepository;
import com.jdbcExample.jdbcExample.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.BDDMockito.*;
import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {



	@Mock
	private ProductRepository productRepository;

	@InjectMocks
	private ProductServiceImpl productServiceImpl;

	@Test
	public void getByIdTest(){
		// given
		Long id = 1L;
		Product product = Product.builder().cost(10).description("Tomate").id(id).build();
		given(productRepository.getById(id)).willReturn(product);
		// when
		Product productFound = productServiceImpl.getById(id);
		// then
		assertThat(productFound).isNotNull();
		assertEquals(product,productFound);
	}
}
